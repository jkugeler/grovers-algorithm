import print from 'print-js'

export function printDocument() {
    try {
        const basePath = window.location.origin + window.location.pathname;
        const fullPath = basePath + 'assets/docs/grovers-poster.pdf';
        printJS(fullPath);
    } catch (error) {
        console.error("Failed to load the document for printing:", error);
        alert("Error: Unable to load the document for printing.");
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const printButton = document.getElementById('printButton');
    if (printButton) {
        printButton.addEventListener('click', printDocument);
    }
});